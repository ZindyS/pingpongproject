package com.example.pingpongprohect

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.core.view.WindowCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Math.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {
    var x = 0f
    var time = 0
    var angle = 0f
    var speed = 8
    val rnd = Random(Date().time)
    var isStart = false
    var down = true
    var left = true
    var right = true
    var up = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        root = this
        setContentView(R.layout.activity_main)
        endGame()
        timeText.animate()
            .setDuration(0)
            .alpha(0f)//
            .start()

        controller.setOnTouchListener { view, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN && !isStart) {
//                ball.animate()
//                    .setDuration(0)
//                    .translationX(500f)
//                    .start()
//                startText.text = ball.x.toString()
                isStart=true
                top5.animate()
                    .setDuration(400)
                    .translationX(800f)
                    .start()
                startText.animate()
                    .setDuration(400)
                    .translationY(-400f)
                    .start()
                timeText.animate()
                    .setDuration(400)
                    .alpha(1f)
                    .start()
                x=motionEvent.x
                time=0
                angle = (220+abs(rnd.nextInt())%101).toFloat()
                Log.d("errror", "angle $angle")
                speed = 18
                up=true
                right=true
                left=true
                down=true
            } else if (motionEvent.action == MotionEvent.ACTION_MOVE) {
                var dist = motionEvent.x-x
                if (player.x+dist < 0 || player.x+dist>670) {
                    dist=0f
                }
                player.animate()
                    .setDuration(0)
                    .translationXBy(dist)
                    .start()
                x=motionEvent.x
            } else if (motionEvent.action == MotionEvent.ACTION_UP) {
                x=0f
            } else if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                x=motionEvent.x
            }
            return@setOnTouchListener true
        }

        val handler = Handler()
        Thread {
            val runnable = object : Runnable {
                override fun run() {
                    if (isStart) {
                        time++
                        ball.animate()
                            .setDuration(0)
                            .translationXBy(speed*cos(angle.toDouble()/180*3.14f).toFloat())
                            .translationYBy(-speed*sin(angle.toDouble()/180*3.14f).toFloat())
                            .withEndAction {
                                if (ball.y in 1765.0..1820.0 && ball.x+80>player.x && ball.x<player.x+400 && down) {
                                    angle=360-angle+10-(rnd.nextInt())%5
                                    up=true
                                    right=true
                                    left=true
                                    down=false
                                } else if (ball.y <= 0 && up) {
                                    angle=360-angle+10-(rnd.nextInt())%5
                                    speed++
                                    up=false
                                    right=true
                                    left=true
                                    down=true
                                } else if (ball.y>=deadzone.y) {
                                    CustomDialog((time*20).toLong()).show(supportFragmentManager, "1")
                                    isStart=false
                                }
                                if (ball.x <= 0 && left) {
                                    angle=180-angle+10-(rnd.nextInt())%5
                                    up=true
                                    right=true
                                    left=false
                                    down=true
                                } else if (ball.x >= 998 && right) {
                                    angle=180-angle+10-(rnd.nextInt())%5
                                    up=true
                                    right=false
                                    left=true
                                    down=true
                                }

                            }
                            .start()
                        timeText.text = SimpleDateFormat("mm:ss").format(Date((time*20).toLong()))
                    }
                    handler.postDelayed(this, 20)
                }
            }
            handler.post(runnable)
        }.start()
    }

    companion object {
        lateinit var root: Activity
        fun endGame() {
            root.top5.animate()
                .setDuration(0)
                .translationX(0f)
                .start()
            root.startText.animate()
                .setDuration(0)
                .translationY(0f)
                .start()
            root.timeText.animate()
                .setDuration(0)
                .alpha(0f)
                .start()
            root.ball.animate()
                .setDuration(0)
                .translationY(0f)
                .translationX(0f)
                .start()
            root.player.animate()
                .setDuration(0)
                .translationX(0f)
                .start()
            val sh = root.getSharedPreferences("MyGame1", 0)
            for (i in 1..5) {
                var name = sh.getString("name$i", "---")
                var time = sh.getLong("time$i", -1)
                var stime = SimpleDateFormat("mm:ss").format(Date(time))
                if (time==-1L) {
                    stime="---"
                    name="---"
                }
                if (i==1) {
                    root.name1.text = name
                    root.time1.text = stime
                } else if (i==2) {
                    root.name2.text = name
                    root.time2.text = stime
                } else if (i==3) {
                    root.name3.text = name
                    root.time3.text = stime
                } else if (i==4) {
                    root.name4.text = name
                    root.time4.text = stime
                } else if (i==5) {
                    root.name5.text = name
                    root.time5.text = stime
                }
            }
        }
    }
}