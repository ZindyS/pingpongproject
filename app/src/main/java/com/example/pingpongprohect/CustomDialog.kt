package com.example.pingpongprohect

import android.os.Bundle
import android.view.*
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.smt.*
import kotlinx.android.synthetic.main.smt.view.*

class CustomDialog(val time : Long) : DialogFragment() {
    override fun onStart() {
        super.onStart()
        dialog!!.setCancelable(false)
        val width = (resources.displayMetrics.widthPixels * 0.9).toInt()
        val height = (resources.displayMetrics.heightPixels * 0.4).toInt()
        dialog!!.window!!.setLayout(width, height)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.smt, container, false)
        root.editTextTextPersonName.addTextChangedListener {
            if (it!!.toString() == "") {
                root.next.text = "Сохранить анонимно"
            } else {
                root.next.text = "Сохранить"
            }
        }
        root.next.setOnClickListener {
            var str = editTextTextPersonName.text.toString()
            if (str=="") { str="Anon" }
            val sh = root.context.getSharedPreferences("MyGame1", 0)
            val ed = sh.edit()
            for (i in 1..5) {
                if (time > sh.getLong("time$i", -1)) {
                    var lastName=str
                    var lastTime=time
                    for (j in i..5) {
                        var tmpN=sh.getString("name$j", "---")
                        var tmpT=sh.getLong("time$j", -1)
                        ed.putString("name$j", lastName)
                        ed.putLong("time$j", lastTime)
                        ed.commit()
                        lastName=tmpN!!
                        lastTime=tmpT
                    }
                    break
                }
            }
            MainActivity.endGame()
            dialog!!.cancel()
        }
        return root
    }
}